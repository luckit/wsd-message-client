package eu.iq.messenger.xmpp;

import eu.iq.messenger.entities.Account;

public interface OnStatusChanged {
	public void onStatusChanged(Account account);
}
