package eu.iq.messenger.xmpp;

import eu.iq.messenger.entities.Account;
import eu.iq.messenger.xmpp.stanzas.IqPacket;

public interface OnIqPacketReceived extends PacketReceived {
	public void onIqPacketReceived(Account account, IqPacket packet);
}
