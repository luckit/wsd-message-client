package eu.iq.messenger.xmpp.jingle;

import eu.iq.messenger.entities.Account;
import eu.iq.messenger.xmpp.PacketReceived;
import eu.iq.messenger.xmpp.jingle.stanzas.JinglePacket;

public interface OnJinglePacketReceived extends PacketReceived {
	void onJinglePacketReceived(Account account, JinglePacket packet);
}
