package eu.iq.messenger.xmpp.jingle;

import eu.iq.messenger.entities.DownloadableFile;

public interface OnFileTransmissionStatusChanged {
	void onFileTransmitted(DownloadableFile file);

	void onFileTransferAborted();
}
