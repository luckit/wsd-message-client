package eu.iq.messenger.xmpp;

import eu.iq.messenger.entities.Contact;

public interface OnContactStatusChanged {
	public void onContactStatusChanged(final Contact contact, final boolean online);
}
