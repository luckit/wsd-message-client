package eu.iq.messenger.xmpp;

import eu.iq.messenger.entities.Account;

public interface OnMessageAcknowledged {
	public void onMessageAcknowledged(Account account, String id);
}
