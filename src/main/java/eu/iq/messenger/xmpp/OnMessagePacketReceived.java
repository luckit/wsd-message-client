package eu.iq.messenger.xmpp;

import eu.iq.messenger.entities.Account;
import eu.iq.messenger.xmpp.stanzas.MessagePacket;

public interface OnMessagePacketReceived extends PacketReceived {
	public void onMessagePacketReceived(Account account, MessagePacket packet);
}
