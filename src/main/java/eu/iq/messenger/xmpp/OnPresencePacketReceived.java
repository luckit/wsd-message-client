package eu.iq.messenger.xmpp;

import eu.iq.messenger.entities.Account;
import eu.iq.messenger.xmpp.stanzas.PresencePacket;

public interface OnPresencePacketReceived extends PacketReceived {
	public void onPresencePacketReceived(Account account, PresencePacket packet);
}
