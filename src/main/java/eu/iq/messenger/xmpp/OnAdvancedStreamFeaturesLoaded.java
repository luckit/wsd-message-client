package eu.iq.messenger.xmpp;

import eu.iq.messenger.entities.Account;

public interface OnAdvancedStreamFeaturesLoaded {
	public void onAdvancedStreamFeaturesAvailable(final Account account);
}
